package gest_compet.model;

public class Discipline {
	private int idDiscipline;
	private String libelle;
	
	public Discipline() {
		super();
	}

	public Discipline(String libelle) {
		super();
		this.libelle = libelle;
	}

	public Discipline(int idDiscipline, String libelle) {
		super();
		this.idDiscipline = idDiscipline;
		this.libelle = libelle;
	}

	public int getIdDiscipline() {
		return idDiscipline;
	}

	public void setIdDiscipline(int idDiscipline) {
		this.idDiscipline = idDiscipline;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	
	

	

}
