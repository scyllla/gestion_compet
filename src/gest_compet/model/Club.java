package gest_compet.model;

public class Club {
private int id ;
private String nomClub;
private String nomAthlete;

public Club() {
	super();
}

public Club(String nomClub, String nomAthlete) {
	super();
	this.nomClub = nomClub;
	this.nomAthlete = nomAthlete;
}

public Club(int id, String nomClub, String nomAthlete) {
	super();
	this.id = id;
	this.nomClub = nomClub;
	this.nomAthlete = nomAthlete;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getNomClub() {
	return nomClub;
}

public void setNomClub(String nomClub) {
	this.nomClub = nomClub;
}

public String getNomAthlete() {
	return nomAthlete;
}

public void setNomAthlete(String nomAthlete) {
	this.nomAthlete = nomAthlete;
} 




}
