package gest_compet.model;

public class Competition {
	private int idCompetition;
	private String nomCompetition;
	private String nbreparticipants;
	private String infoCompetition;
	
	public Competition() {
		super();
	}

	public Competition(String nomCompetition, String nbreparticipants, String infoCompetition) {
		super();
		this.nomCompetition = nomCompetition;
		this.nbreparticipants = nbreparticipants;
		this.infoCompetition = infoCompetition;
	}

	public Competition(int idCompetition, String nomCompetition, String nbreparticipants, String infoCompetition) {
		super();
		this.idCompetition = idCompetition;
		this.nomCompetition = nomCompetition;
		this.nbreparticipants = nbreparticipants;
		this.infoCompetition = infoCompetition;
	}

	public int getIdCompetition() {
		return idCompetition;
	}

	public void setIdCompetition(int idCompetition) {
		this.idCompetition = idCompetition;
	}

	public String getNomCompetition() {
		return nomCompetition;
	}

	public void setNomCompetition(String nomCompetition) {
		this.nomCompetition = nomCompetition;
	}

	public String getNbreparticipants() {
		return nbreparticipants;
	}

	public void setNbreparticipants(String nbreparticipants) {
		this.nbreparticipants = nbreparticipants;
	}

	public String getInfoCompetition() {
		return infoCompetition;
	}

	public void setInfoCompetition(String infoCompetition) {
		this.infoCompetition = infoCompetition;
	}
	
	

}
