package gest_compet.model;

public class Calendrier {
	private int id;
	private String libelle;
	public Calendrier() {
		super();
	}
	public Calendrier(String libelle) {
		super();
		this.libelle = libelle;
	}
	public Calendrier(int id, String libelle) {
		super();
		this.id = id;
		this.libelle = libelle;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	
	

}
